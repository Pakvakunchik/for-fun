const getSleepHours = (day) =>{
  day = day.toLowerCase();
  switch(day) {
    case 'monday':
      return 8;
      break;
    case 'tuesday':
      return 7;
      break;
    case 'wednesday':
      return 6;
      break;
    case 'thursday':
      return 9;
      break;
    case 'friday':
      return 5;
      break;
    case 'saturday':
      return 11;
      break;
    case 'sunday':
      return 10;
      break;
  }
}
const getActualSleepHours = () => {
  let arrOfDays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

  return arrOfDays.reduce( (acc, item) =>  acc +=  getSleepHours(item) ,0)
} 

const getIdealSleepHours = (hours) => {let idealHours = hours * 7; return idealHours}

const calculateSleepDebt = (hours) => {
  hours = prompt('your perfect sleep time?', '')
  if(getActualSleepHours()===getIdealSleepHours(hours)){
    return 'perfect amount of sleep'
  }else if(getActualSleepHours()>getIdealSleepHours(hours)){
    return 'more sleep than needed for ' + (getActualSleepHours() - getIdealSleepHours(hours)) + ' hours'
  }else {
    return 'should get some rest for ' + (getIdealSleepHours(hours) -  getActualSleepHours()) + ' hours';
  }
  
}

alert(calculateSleepDebt())