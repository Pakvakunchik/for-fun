const getUserChoise = userInput => {
  userInput = userInput.toLowerCase();
  
  if(userInput === 'rock' || userInput ==='paper' || userInput ==='scissors'){
    return userInput;
  } else {
    console.log('error')
  }
}

const getComputerChoise = () =>{
  a = Math.floor(Math.random(2)*3)
  
  a === 0 ? a = 'rock': a === 1 ? a = 'paper': a = 'scissors';
  
  return a
}

const determinateWinner = (userChoise, computerChoise) =>{
  if (userChoise === computerChoise) {
  	return 'tie' 
  } else {
    if (userChoise === 'rock'){
      if(computerChoise === 'paper'){
        return 'computer won'
      }else{
        return 'user won'
      }
    }
    
  if (userChoise === 'paper'){
      if(computerChoise === 'scissors'){
        return 'computer won'
      }else{
        return 'user won'
      }
  }
    
	if (userChoise === 'scissors'){
      if(computerChoise === 'rock'){
        return 'computer won'
      }else{
        return 'user won'
      }
      }
}
}

const playGame = () => {
  let userChoise = getUserChoise(prompt('rock, paper or scissors? ', 'rock')),
  		computerChoise = getComputerChoise();
  console.log('user' + userChoise)
  console.log('computer' + computerChoise)
  return determinateWinner(userChoise, computerChoise)
}

playGame();